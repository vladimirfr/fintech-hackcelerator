name := "fintech-hackcelerator"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.apache.commons" % "commons-compress" % "1.14",
  "com.google.guava" % "guava" % "19.0",

  "org.apache.tika" % "tika-app" % "1.16",
  "com.levigo.jbig2" % "levigo-jbig2-imageio" % "1.6.5", // Support for JBIG2 images - TIKA/PDFBOX
  "com.github.jai-imageio" % "jai-imageio-core" % "1.3.1", // Support for JPX and TIFF - TIKA/PDFBOX

  "org.apache.hbase" % "hbase-common" % "1.1.2",
  "org.apache.hbase" % "hbase-client" % "1.1.2",
  "org.apache.hbase" % "hbase-server" % "1.1.2" excludeAll(ExclusionRule(organization = "org.eclipse.jetty")),
  "org.apache.hbase" % "hbase-protocol" % "1.1.2",
  "org.apache.hadoop" % "hadoop-core" % "1.1.2" % "provided" exclude("org.apache.commons", "commons-compress"),
  "org.apache.hbase" % "hbase-hadoop-compat" % "1.1.2",
  "com.typesafe" % "config" % "1.3.0",
  "org.apache.spark" % "spark-core_2.11" % "2.1.1",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.8.0",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.8.0" classifier "models",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.8.0" classifier "models-german",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.8.0" classifier "models-french"
)

