package fintech.hackcelerator

import fintech.hackcelerator.config.AppConfig
import fintech.hackcelerator.database.hbase.HBaseWriter
import fintech.hackcelerator.parsers.TikaParser
import fintech.hackcelerator.tools.{AppSpark, Dataset, LogHelper, PreprocessText}
import org.apache.hadoop.conf.Configuration
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object IDriver extends LogHelper with java.io.Serializable {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    val base_path: String = AppConfig.dataset
    val sc: SparkContext = AppSpark.getContext()
    val sections: Array[RDD[String]] = Dataset.getFilesBySections(sc,base_path)

    val conf: Configuration = sc.hadoopConfiguration
    val fs: org.apache.hadoop.fs.FileSystem = org.apache.hadoop.fs.FileSystem.get(conf)

    for(i <- 0 until sections.length){
      val section = sections(0)
      println("IDriverINFO: Start parsing files....")
      val files: RDD[Map[String, Any]] = section.map(x => TikaParser.extractFuture(x, fs))
      files.persist(AppSpark.cacheLevel)

      files.take(5).foreach(println)

      println("IDriverINFO: Start saving files info....")
      HBaseWriter.saveDataset(files,i.toString)

      println("IDriverINFO: Start saving files info....")
      val contents: RDD[(String, String, Int, Int, Int, String)] = files.map(x => (x.get("file").mkString, x.get("language").mkString, x.get("content").mkString))
        .filter(x => x._3.length>1)
        .map(x => (x._1, x._2, PreprocessText.getParagraphs(x._3)))
        .map(x => (x._1, x._2, PreprocessText.paragraphsWithIndexes(x._3)))
        .flatMap(x => x._3.map(y => (x._1, x._2, y._1, y._2)))
        .map(x => (x._1, x._2, x._3, PreprocessText.getSentencesWithDist(x._4)))
        .flatMap(x => x._4.map(y => (x._1, x._2, x._3, y._1, y._2, y._3)))
        .persist(AppSpark.cacheLevel)

      contents.take(5).foreach(println)
      contents.map(x => s"${x._1}IDriverSeparator${x._2}IDriverSeparator${x._3}IDriverSeparator${x._4}IDriverSeparator${x._5}IDriverSeparator${x._6}")
        .saveAsTextFile(s"${AppConfig.parsed_text}/section-${i}.csv")

      files.unpersist()
      contents.unpersist()
    }
  }
}
