package fintech.hackcelerator

import fintech.hackcelerator.config.AppConfig
import fintech.hackcelerator.database.hbase.HBaseWriter
import fintech.hackcelerator.ner.{DataTypesDetector, OrganizationDetector, PersonDetector}
import fintech.hackcelerator.tools.{AppSpark, LogHelper}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD

object NERFind extends LogHelper with java.io.Serializable {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    AppSpark.appName = "NERFind"
    val sc = AppSpark.getContext()

    val conf: Configuration = sc.hadoopConfiguration
    val fs: org.apache.hadoop.fs.FileSystem = org.apache.hadoop.fs.FileSystem.get(conf)
    val sections: Array[String] = fs.listStatus(new Path(AppConfig.parsed_text)).map(x => x.getPath.toString)

    for(section: String <- sections){
      val content: RDD[(String, String, Int, Int, Int, String)] = sc.textFile(section, AppConfig.partitions).map(_.split("IDriverSeparator"))
        .filter(_.length == 6)
        .map(x => (x(0), x(1), x(2).toInt, x(3).toInt, x(4).toInt, x(5))) //(file, language, paragraph, sentece, dist, text)
        .persist(AppSpark.cacheLevel)

      val datatypes: RDD[(String, String, Int, Int, Int, List[(String, String, List[Int])])] = content.map(x =>{
        (x._1, x._2, x._3, x._4, x._5, DataTypesDetector.find(x._6 ))
      }).persist(AppSpark.cacheLevel)
      HBaseWriter.saveDatatypes(datatypes)
      datatypes.unpersist()

      val organizations: RDD[(String, String, Int, Int, Int, List[(String, List[Int])])] = content.map(x =>{
        (x._1, x._2, x._3, x._4, x._5, OrganizationDetector.find(x._6 ))
      }).persist(AppSpark.cacheLevel)
      HBaseWriter.saveOrganizations(organizations)
      organizations.unpersist()

      val languages: Array[String] = content.map(x => x._2).distinct().collect()
      for(language <- languages){
        val sub_content: RDD[(String, String, Int, Int, Int, String)] = content.filter(x => x._2 == language)
          .persist(AppSpark.cacheLevel)

        var personDetector = new PersonDetector(AppConfig.getDefaultLanguage(language))

        val persons: RDD[(String, String, Int, Int, Int, List[(String, String, List[Int])])] = content.map(x =>{
          (x._1, x._2, x._3, x._4, x._5, personDetector.find(x._6))
        }).persist(AppSpark.cacheLevel)
        HBaseWriter.savePersons(persons)
        persons.unpersist()
        personDetector

        sub_content.unpersist()
        personDetector = null
      }
    }
  }
}
