package fintech.hackcelerator.config

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.hadoop.hbase.HBaseConfiguration

object AppConfig {
  val config: Config = ConfigFactory.parseFile(new File("Dathena.conf"))

  //config
  val hostname: String = config.getString("hostname")
  val ZOOKEEPER: String = hostname.trim + ":2181"
  val HBASESERVER: String = hostname.trim + ":16000"
  val HBASEROOTDIR: String = "hdfs://" + hostname.trim + ":8020/apps/hbase/data"

  //spark
  val deploy_mode: String = config.getString("deploy-mode")
  val cache_level: String = config.getString("cache-level")
  val num_executors: String = config.getString("num-executors")
  val driver_memory: String = config.getString("driver-memory")
  val executor_memory: String = config.getString("executor-memory")
  val executor_cores: String = config.getString("executor-cores")
  val spark_yarn_am_cores: String = config.getString("spark.yarn.am.cores")
  val spark_serializer: String = config.getString("spark-serializer")
  val partitions: Int = config.getInt("partitions")

  //base paths
  lazy val dataset: String = config.getString("dataset")
  lazy val file_listing_path: String = config.getString("file-listing-path")
  lazy val parsed_text: String = config.getString("parsed-text")

  //settings for load files
  lazy val section_batch: Long = config.getLong("section-batch")
  lazy val hashingAlgorithm: String = config.getString("hashingAlgorithm")
  lazy val ner_n_gram_email:Int = config.getInt("ner-n-gram-email")
  lazy val ner_n_gram_website:Int = config.getInt("ner-n-gram-website")
  lazy val ner_dist_email:Int = config.getInt("ner-dist-email")
  lazy val ner_dist_website:Int = config.getInt("ner-dist-website")
  lazy val ner_dist_default:Int = config.getInt("ner-dist-default")
  lazy val is_individual_matching: Int = config.getInt("is-individual-matching")
  lazy val is_individual_pii_matching: Int = config.getInt("is-individual-pii-matching")
  lazy val ner_freq_limit: Int = config.getInt("ner-freq-limit")
  lazy val ner_nouns_probability: Float = config.getDouble("ner-nouns-probability").toFloat

  //parser settings
  lazy val defaultLanguage = "en"
  lazy val maxTime: Int = config.getInt("maxTime")
  def ocrMode: String = config.getString("ocrMode")  // no_ocr, ocr_only, ocr_and_text

  def getOcrMode: String = ocrMode
  def getDefaultLanguage(lang: String): String = {
    if(config.getString("default-languages").split(',').contains(lang)){
      lang
    }else{
      defaultLanguage
    }
  }

  lazy val openNLPModels = config.getString("open-nlp-models")

  def getConfig(): org.apache.hadoop.conf.Configuration = {
    val config: org.apache.hadoop.conf.Configuration = HBaseConfiguration.create()
    config.set("hbase.zookeeper.quorum", ZOOKEEPER)
    config.set("hbase.master", HBASESERVER)
    config.set("zookeeper.znode.parent", "/hbase-unsecure")
    config.set("zookeeper.rootdir", HBASEROOTDIR)
    config.set("fs.defaultFS", s"hdfs://${hostname}:8020")
    return config
  }
}
