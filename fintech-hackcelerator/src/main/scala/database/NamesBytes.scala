package fintech.hackcelerator.database

import org.apache.hadoop.hbase.util.Bytes

object NamesBytes {
  lazy val informationBytes: Array[Byte] = Bytes.toBytes("Information")
  lazy val encryptedBytes: Array[Byte] = Bytes.toBytes("encrypted")
  lazy val exceptionBytes: Array[Byte] = Bytes.toBytes("has_exception")
  lazy val filenameBytes: Array[Byte] = Bytes.toBytes("filename")
  lazy val metadataBytes: Array[Byte] = Bytes.toBytes("metadata")
  lazy val modifiedBytes: Array[Byte] = Bytes.toBytes("lastmodified")
  lazy val modifierBytes: Array[Byte] = Bytes.toBytes("lastModifier")
  lazy val ownerBytes: Array[Byte] = Bytes.toBytes("owner")
  lazy val remotePathBytes: Array[Byte] = Bytes.toBytes("remotePath")
  lazy val section_indexBytes: Array[Byte] = Bytes.toBytes("section_index")
  lazy val languageBytes: Array[Byte] = Bytes.toBytes("language")
  lazy val creationBytes: Array[Byte] = Bytes.toBytes("CreationDate")
  lazy val doctypeBytes: Array[Byte] = Bytes.toBytes("docType")
}
