package fintech.hackcelerator.database

object TableNames{
  lazy val namespace: String = "Hackcelerator"

  lazy val docmeta: String = s"$namespace:DOCMETA"
  lazy val datatypes: String = s"$namespace:DataTypes"
  lazy val persons: String = s"$namespace:Persons"
  lazy val organizations: String = s"$namespace:Organizations"
  lazy val persons_information: String = s"$namespace:Persons_Information"
}