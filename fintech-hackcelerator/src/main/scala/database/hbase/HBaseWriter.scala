package fintech.hackcelerator.database.hbase

import fintech.hackcelerator.config.AppConfig
import fintech.hackcelerator.database.NamesBytes._
import fintech.hackcelerator.database.TableNames
import fintech.hackcelerator.tools.Tools
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{BufferedMutator, ConnectionFactory, Put}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.rdd.RDD

object HBaseWriter {
  def saveDataset(data: RDD[Map[String, Any]], section_index: String = "0"): Unit = {
    data.foreachPartition {
      iter =>
        val connection: org.apache.hadoop.hbase.client.Connection = ConnectionFactory.createConnection(AppConfig.getConfig())
        val mutator: BufferedMutator = connection.getBufferedMutator(TableName.valueOf(TableNames.docmeta))
        iter.foreach(
          x => {
            try {
              val path: String = x.get("file").mkString.stripPrefix(AppConfig.dataset)
              var language: String = x.get("language").mkString

              var has_exception: String = "true"
              if (x.get("content").mkString.split(' ').length > 1) {
                has_exception = "false"
              }

              val extension: String = x.get("extension").mkString.toLowerCase

              val p: Put = new Put(Bytes.toBytes(Tools.hashString(path)))
              p.addColumn(informationBytes, remotePathBytes, Bytes.toBytes(path))
              p.addColumn(informationBytes, encryptedBytes, Bytes.toBytes(x.get("encrypted").mkString))
              p.addColumn(informationBytes, exceptionBytes, Bytes.toBytes(has_exception))
              p.addColumn(informationBytes, Bytes.toBytes("exception_type"), Bytes.toBytes(x.get("has_exception").mkString))
              p.addColumn(informationBytes, modifiedBytes, Bytes.toBytes(x.get("lastmodified").mkString))
              p.addColumn(informationBytes, creationBytes, Bytes.toBytes(x.get("creationdate").mkString))
              p.addColumn(informationBytes, doctypeBytes, Bytes.toBytes(x.get("docType").mkString))
              p.addColumn(informationBytes, Bytes.toBytes("extension"), Bytes.toBytes(Tools.matchDocType(extension, true)))
              p.addColumn(informationBytes, metadataBytes, Bytes.toBytes(x.get("metadata").mkString))
              p.addColumn(informationBytes, languageBytes, Bytes.toBytes(language))
              p.addColumn(informationBytes, section_indexBytes, Bytes.toBytes(section_index.toString))
              p.addColumn(informationBytes, ownerBytes, Bytes.toBytes(x.get("creator").mkString))
              p.addColumn(informationBytes, modifierBytes, Bytes.toBytes(x.get("lastmodifier").mkString))
              p.addColumn(informationBytes, Bytes.toBytes("textDocType"), Bytes.toBytes(x.get("textDocType").mkString))
              mutator.mutate(p)
            }
            catch {
              case e: Throwable => e.printStackTrace(System.err)
            }
          }
        )
    }
  }

  def saveDatatypes(data: RDD[(String, String, Int, Int, Int, List[(String, String, List[Int])])]): Unit = {
    data.foreachPartition {
      iter =>
        val connection: org.apache.hadoop.hbase.client.Connection = ConnectionFactory.createConnection(AppConfig.getConfig())
        val mutator: BufferedMutator = connection.getBufferedMutator(TableName.valueOf(TableNames.datatypes))
        iter.foreach(
          x => {
            try {
              val path: String = x._1.stripPrefix(AppConfig.dataset)

              for(datatype <- x._6) {
                for(p <- datatype._3) {

                  val p: Put = new Put(Bytes.toBytes(path))
                  p.addColumn(informationBytes, languageBytes, Bytes.toBytes(x._2))
                  p.addColumn(informationBytes, remotePathBytes, Bytes.toBytes(path))
                  p.addColumn(informationBytes, Bytes.toBytes("paragraph"), Bytes.toBytes(x._3.toString))
                  p.addColumn(informationBytes, Bytes.toBytes("sentence"), Bytes.toBytes(x._4.toString))
                  p.addColumn(informationBytes, Bytes.toBytes("offset"), Bytes.toBytes(x._5.toString))

                  p.addColumn(informationBytes, Bytes.toBytes("value"), Bytes.toBytes(datatype._1))
                  p.addColumn(informationBytes, Bytes.toBytes("rule"), Bytes.toBytes(datatype._2))
                  p.addColumn(informationBytes, Bytes.toBytes("position"), Bytes.toBytes(p.toString))

                  mutator.mutate(p)
                }
              }
            }
            catch {
              case e: Throwable => e.printStackTrace(System.err)
            }
          }
        )
    }
  }

  def savePersons(data: RDD[(String, String, Int, Int, Int, List[(String, String, List[Int])])]): Unit = {
    data.foreachPartition {
      iter =>
        val connection: org.apache.hadoop.hbase.client.Connection = ConnectionFactory.createConnection(AppConfig.getConfig())
        val mutator: BufferedMutator = connection.getBufferedMutator(TableName.valueOf(TableNames.persons))
        iter.foreach(
          x => {
            try {
              val path: String = x._1.stripPrefix(AppConfig.dataset)

              for(person <- x._6) {
                for (p <- person._3) {

                  val p: Put = new Put(Bytes.toBytes(path))
                  p.addColumn(informationBytes, languageBytes, Bytes.toBytes(x._2))
                  p.addColumn(informationBytes, remotePathBytes, Bytes.toBytes(path))
                  p.addColumn(informationBytes, Bytes.toBytes("paragraph"), Bytes.toBytes(x._3.toString))
                  p.addColumn(informationBytes, Bytes.toBytes("sentence"), Bytes.toBytes(x._4.toString))
                  p.addColumn(informationBytes, Bytes.toBytes("offset"), Bytes.toBytes(x._5.toString))

                  p.addColumn(informationBytes, Bytes.toBytes("value"), Bytes.toBytes(person._1))
                  p.addColumn(informationBytes, Bytes.toBytes("rule"), Bytes.toBytes(person._2))
                  p.addColumn(informationBytes, Bytes.toBytes("position"), Bytes.toBytes(p.toString))

                  mutator.mutate(p)
                }
              }
            }
            catch {
              case e: Throwable => e.printStackTrace(System.err)
            }
          }
        )
    }
  }

  def saveOrganizations(data: RDD[(String, String, Int, Int, Int, List[(String, List[Int])])]): Unit = {
    data.foreachPartition {
      iter =>
        val connection: org.apache.hadoop.hbase.client.Connection = ConnectionFactory.createConnection(AppConfig.getConfig())
        val mutator: BufferedMutator = connection.getBufferedMutator(TableName.valueOf(TableNames.organizations))
        iter.foreach(
          x => {
            try {
              val path: String = x._1.stripPrefix(AppConfig.dataset)

              for(person <- x._6) {
                for (p <- person._2) {

                  val p: Put = new Put(Bytes.toBytes(path))
                  p.addColumn(informationBytes, languageBytes, Bytes.toBytes(x._2))
                  p.addColumn(informationBytes, remotePathBytes, Bytes.toBytes(path))
                  p.addColumn(informationBytes, Bytes.toBytes("paragraph"), Bytes.toBytes(x._3.toString))
                  p.addColumn(informationBytes, Bytes.toBytes("sentence"), Bytes.toBytes(x._4.toString))
                  p.addColumn(informationBytes, Bytes.toBytes("offset"), Bytes.toBytes(x._5.toString))

                  p.addColumn(informationBytes, Bytes.toBytes("value"), Bytes.toBytes(person._1))
                  p.addColumn(informationBytes, Bytes.toBytes("rule"), Bytes.toBytes("ORGANIZATION LIST"))
                  p.addColumn(informationBytes, Bytes.toBytes("position"), Bytes.toBytes(p.toString))

                  mutator.mutate(p)
                }
              }
            }
            catch {
              case e: Throwable => e.printStackTrace(System.err)
            }
          }
        )
    }
  }
}
