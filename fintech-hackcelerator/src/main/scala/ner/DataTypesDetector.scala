package fintech.hackcelerator.ner

import fintech.hackcelerator.tools.PreprocessText

import scala.collection.mutable.ListBuffer
import scala.util.matching.Regex

object DataTypesDetector {
  private val IBAN: String = """\b(NO[0-9]{2}(?:[ -]?[0-9A-Z]{4}){2}[ -]?[0-9A-Z]{3}|BE[0-9]{2}(?:[ -]?[0-9A-Z]{4}){3}|(DK|FO|FI|GL|NL)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){3}[ -]?[0-9A-Z]{2}|(MK|SI)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){3}[ -]?[0-9A-Z]{3}|(BA|EE|KZ|LT|LU|AT)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){4}|(HR|LI|LV|CH)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){4}[ -]?[0-9A-Z]|(BG|DE|IE|ME|RS|GB|GE)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){4}[ -]?[0-9A-Z]{2}|(GI|IL)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){4}[ -]?[0-9A-Z]{3}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){5}|PT[0-9]{2}(?:[ -]?[0-9A-Z]{4}){5}[ -]?[0-9A-Z]|(IS|TR)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){5}[ -]?[0-9A-Z]{2}|(FR|GR|IT|MC|SM)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){5}[ -]?[0-9A-Z]{3}|(AL|CY|HU|LB|PL)[0-9]{2}(?:[ -]?[0-9A-Z]{4}){6}|MU[0-9]{2}(?:[ -]?[0-9A-Z]{4}){6}[ -]?[0-9A-Z]{2}|MT[0-9]{2}(?:[ -]?[0-9A-Z]{4}){6}[ -]?[0-9A-Z]{3})\b"""
  private val SWIFTCODE: String = """\b[A-Z]{4}(?:A[DEFGILMOQRSTUWXZ]|B[ABDEFGHIJLMNOQRSTVWYZ]|C[ACDFGHIKLMNORUVWXYZ]|D[EJKMOZ]|E[CEGHRST]|F[IJKMOR]|G[ABDEFGHILMNPQRSTUWY]|H[KMNRTU]|I[DELMNOQRST]|J[EMOP]|K[EGHIMNPRWYZ]|L[ABCIKRSTUVY]|M[ACDEFGHKLMNOPQRSTUVWXYZ]|N[ACEFGILOPRUZ]|OM|P[AEFGHKLMNRSTWY]|QA|R[EOSUW]|S[ABCDEGHIJKLMNORSTVXYZ]|T[CDFGHJKLMNORTVWZ]|U[AGMSYZ]|V[ACEGINU]|V[GINU]|W[FS]|Y[ET]|Z[AMW])[A-Z]{2}(?:[A-Z0-9]{3})?\b"""

  private val Passports: String = """\bP[0-9A-Z<](ABW|AFG|AGO|AIA|ALB|AND|ANT|ARE|ARG|ARM|ASM|ATA|ATG|AUS|AUT|AZE|BDI|BEL|BEN|BFA|BGD|BGR|BHR|BHS|BIH|BLR|BLZ|BMU|BOL|BRA|BRB|BRN|BTN|BVT|BWA|CAF|CAN|CCK|CHE|CHL|CHN|CIV|CMR|COD|COG|COK|COL|COM|CPV|CRI|CUB|CXR|CYM|CYP|CZE|D<<|DJI|DMA|DNK|DOM|DZA|ECU|EGY|ERI|ESH|ESP|EST|ETH|FIN|FJI|FLK|FRA|FRO|FSM|FXX|GAB|GBD|GBN|GBO|GBP|GBR|GBS|GEO|GHA|GIB|GIN|GLP|GMB|GNB|GNQ|GRC|GRD|GRL|GTM|GUF|GUM|GUY|HKG|HMD|HND|HRV|HTI|HUN|IDN|IND|IOT|IRL|IRN|IRQ|ISL|ISR|ITA|JAM|JOR|JPN|KAZ|KEN|KGZ|KHM|KIR|KNA|KOR|KWT|LAO|LBN|LBR|LBY|LCA|LIE|LKA|LSO|LTU|LUX|LVA|MAR|MCO|MDA|MDG|MDV|MEX|MHL|MKD|MLI|MLT|MMR|MNG|MNP|MOZ|MRT|MSR|MTQ|MUS|MWI|MYS|MYT|NAM|NCL|NER|NFK|NGA|NIC|NIU|NLD|NOR|NPL|NRU|NTZ|NZL|OMN|PAK|PAN|PCN|PER|PHL|PLW|PNG|POL|PRI|PRK|PRT|PRY|PYF|QAT|REU|ROM|RUS|RWA|SAU|SDN|SEN|SGP|SGS|SHN|SJM|SLB|SLE|SLV|SMR|SOM|SPM|STP|SUR|SVK|SVN|SWE|SWZ|SYC|SYR|TCA|TCD|TGO|THA|TJK|TKL|TKM|TMP|TON|TTO|TUN|TUR|TUV|TWN|TZA|UGA|UKR|UMI|UNA|UNO|URY|USA|UZB|VAT|VCT|VEN|VGB|VIR|VNM|VUT|WLF|WSM|XXA|XXB|XXC|XXX|YEM|ZAF|ZAR|ZMB|ZWE)[A-Z<]{39}[A-Z0-9<]{9}[0-9<]\1[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12][0-9]|3[01])[0-9<][MF<][0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12][0-9]|3[01])[0-9<][0-9A-Z<]{14}[0-9<][0-9]\b"""

  private val IPv4: String = """\b(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])(?:\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])){3}\b"""
  private val date_ddmmyyyy: String = """\b(?:0[1-9]|[12][0-9]|3[01])-(?:0[1-9]|1[12])-(?:19[0-9]{2}|20[0-9]{2})\b"""
  private val date_mmddyyyy: String = """\b(?:0[1-9]|1[12])-(?:0[1-9]|[12][0-9]|3[01])-(?:19[0-9]{2}|20[0-9]{2})\b"""
  private val date_yyyymmdd: String = """\b(?:19[0-9]{2}|20[0-9]{2})-(?:0[1-9]|1[12])-(?:0[1-9]|[12][0-9]|3[01])\b"""

  private val passwords: String = """(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*[#$@$!%*?&])[A-Za-z\d$#@!%*?&]{5,15}"""
  private val websites: String = """\b(?:(?:https?:\/\/(?:www\.)?)|(?:www\.))(?:[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|[a-zA-Z0-9]+\.[^\s]{2,})\b"""
  private val email: String = """\b(?:[0-9a-zA-Z]+(?:[-._+]|&amp;))*[0-9a-zA-Z]+(?:@|<at>)(?:[-0-9a-zA-Z]+\.)+[a-zA-Z]{2,6}\b"""

  private val cards: String = """\b(?:3(?:[47]\d(?:[ -]?)\d{4}(?:[- ]?\d{4}){2}|0[0-5]\d{11}|[68]\d{12})|4(?:\d{3})?[ -]?\d{4}(?:[- ]?\d{4}){2}|6011[ -]?\d{4}(?:[- ]?\d{4}){2}|5[1-5]\d\d(?:[- ]?\d{4}){3}|(2014|2149|2131|1800)\d{11}|3\d{15})\b"""

  private val SSN: String = """\b(?!000)(?!666)(?:[0-6]\d{2}|7(?:[0-356]\d|7[012]))-(?!00)\d{2}-(?!0000)\d{4}\b"""
  private val USPhones: String = """(?<=^|\s)((?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4})(?>=$|\s)"""

  private val french_passport: String = """\b[01]\d[A-Z]{2}\d{5}\b"""
  private val french_id: String = """\bIDFRA[A-Z<]{25}([0-9A-Z]{3}|<{3})(?:[0-9]{3}|<{3})(\s|)[0-9]{4}\1[0-9]{5}[0-9][A-Z<]{14}[0-9]{6}[0-9][MF][0-9]\b"""
  private val french_phone: String = """(?:\+33|00\s?33)\s?\d\s?\d\d\s?\d\d\s?\d\d\s?\d\d\b"""
  private val french_insee: String = """\b[12] ?\d\d ?(?:0[1-9]|1[12]) ?(?:\d\d\d?|\d[a-zA-Z]) ?\d\d\d? ?\d\d\d ?(?:[0][1-9]|[1-8][0-9]|9[0-7])\b"""

  private val swissPhone: String = """\b(?:\+|00|0)41(?:\(0\)|)\s?\d\d\s?\d{3}\s?\d\d\s?\d\d\b\b"""
  private val swissOldSSN: String = """\b(?:756\.|)\d{4}\.\d{4}\.\d{2}\b"""
  private val swissSSN: String = """\b\d{3}\.\d{2}\.{3}\.{3}\b"""

  private val bloodType: String = """\b(?:[Bb]lood [Tt]ype|[Gg]roupe [Ss]anguin).{2,10}\b"""
  private val gender: String = """\b(?:[Mm]ale|[Ff]emale|[Hh]omme|[Ff]emme|[Mm]asculin|[Ff]eminin)"""
  private val religions_english: String = """\b(?:[Ii]slam|[Mm]uslim|[Cc]hristianity|[Cc]hristian|[Jj]udaism|[Jj]ew|[Jj]ewish|[Bb]uddhism|[Bb]uddhist|[Ss]unni|[Ss]unnite|[Ss]hi'?ah|[Pp]rotestantism|[Pp]rotestant)\b"""
  private val religions_french: String = """\b(?:[Ii]slam(?:is[mt]e)?|[Mm]usulmane?|[Cc]hretien(?:ne)?|[Jj]ui(?:f|ve)|[Jj]udais[tm]e|[Bb]ouddhis[tm]e|[Ss]unnite|[Cc]hiite|[Pp]rotestante?|[Pp]rotestantisme)\b"""

  private val ethnie_en: String = """\b(?:[Cc]aucasian|[Nn]egro|[Aa]frican|[Mm]ongoloid|[Ee]uropid)\b"""

  private val nric_singapore: String = """\b[STFG]\d{7}[A-Z]\b"""

  private val allStrings: List[(String, String, String)] = List(
    // Worldwide
    ("Passports", "Passports", Passports),
    ("IBAN", "IBAN", IBAN),
    ("SWIFT Code", "SWIFT", SWIFTCODE),
    ("Cards", "Cards", cards),
    ("IPv4", "IPv4", IPv4),
    ("Email", "Email", email),
    ("Websites", "Websites", websites),
    ("Passwords", "Passwords", passwords),
    ("Date DD-MM-YYYY", "Date", date_ddmmyyyy),
    ("Date MM-DD-YYYY", "Date", date_mmddyyyy),
    ("Date YYYY-MM-DD", "Date", date_yyyymmdd),
    ("Blood Type", "Blood Type", bloodType),
    ("Gender", "Gender", gender),
    ("Ethnie EN", "Ethnie", ethnie_en),

    // Language specific
    ("Religion FR", "Religion", religions_french),
    ("Religion EN", "Religion", religions_english),

    // France
    ("French Passports", "Passports", french_passport),
    ("French ID", "ID", french_id),
    ("French Phone", "Phone", french_phone),
    ("French INSEE", "ID", french_insee),

    // US
    ("US Phones", "Phone", USPhones),
    ("SSN", "SSN", SSN),

    // Singapore
    ("NRIC", "ID", nric_singapore),

    // Switzerland
    ("Swiss Phone", "Phone", swissPhone),
    ("Swiss Old SSN", "SSN", swissOldSSN),
    ("Swiss SSN", "SSN", swissSSN)
  )

  private val allRegexes: List[(String, String, Regex)] = allStrings.map(x => (x._1,x._2,new Regex(x._3)))

  def find(text: String): List[(String, String, List[Int])] = {
    var result: ListBuffer[(String, String, List[Int])] = ListBuffer()
    for ((rule, _, regex) <- allRegexes) {
      for (pii <- regex.findAllIn(text)) {
        result += ((pii.trim, rule, PreprocessText.findAllPositions(pii, text)))
      }
    }
    result.toList.distinct
  }

}
