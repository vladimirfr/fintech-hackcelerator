package fintech.hackcelerator.ner

import fintech.hackcelerator.tools.PreprocessText

object OrganizationDetector {
  val org_list = List(
    "DBS Bank",
    "IE Singapore",
    "Temasek",
    "Prudential",
    "UOBPrivate Banking & Trust",
    "UOBPrivate Banking and Trust",
    "UOBPrivate Banking&Trust",
    "Singapore Airlines",
    "Singtel",
    "Keppel Corp",
    "Shangri la",
    "SPH",
    "Starhub",
    "SATS",
    "Raffles Medical",
    "ComfortDelgro",
    "CapitaMall Trust",
    "Far East Organization",
    "Hong Leong Financial Group",
    "Fortune REIT",
    "Wilmar int’l",
    "Wilmar intl",
    "UOL"
  )

  def find(text:  String): List[(String, List[Int])] = {
    return org_list.map(x => (x, PreprocessText.findAllPositions(x.toLowerCase, text))).filter(x => x._2.length > 0)
  }
}
