package fintech.hackcelerator.ner

import java.io.InputStream

import fintech.hackcelerator.config.AppConfig
import fintech.hackcelerator.nlp._
import fintech.hackcelerator.tools.{PreprocessText, Resources}
import opennlp.tools.tokenize.{TokenizerME, TokenizerModel}

import scala.collection.mutable.ListBuffer
import scala.util.matching.Regex

class PersonDetector(lang: String) {
  val c1: Regex = new Regex("^[^A-Za-z]")
  val c2: Regex = new Regex("[^A-Za-z]$")
  val c3: Regex = new Regex("[A-Z][a-z]+")

  val regex_detector = new RegexNLP(lang)
  val opennlp_detector = new OpenNLP(lang)
  val stanford_detector = new StanfordNLP(lang)
  val nounsDetector = new NounsOpenNLPDetector(lang)

  lazy val names: Array[String] = loadNames(lang)
  lazy val wordFreq: Array[String] = loadWordFreq(lang)
  lazy val swords: Array[String] = loadSWords(lang)

  private def loadNames(lang: String): Array[String] ={
    var inputStream: Option[InputStream] = None
    var names: Array[String] = Array[String]()
    try {
      inputStream = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/$lang-names.csv"))
      names = scala.io.Source.fromInputStream(inputStream.get).getLines().toArray
    } catch {
      case e: Throwable =>
        System.err.println(s"ERROR: Failed to process ${AppConfig.openNLPModels}/$lang-token.names.csv")
        e.printStackTrace(System.err)
        try{
          inputStream = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/${AppConfig.defaultLanguage}-names.csv"))
          names = scala.io.Source.fromInputStream(inputStream.get).getLines().toArray
        }catch {
          case e: Throwable =>
            System.err.println(s"ERROR: Failed to process ${AppConfig.openNLPModels}/$lang-names.csv token")
            e.printStackTrace(System.err)
        }
    } finally {
      inputStream match {
        case Some(stream) => stream.close()
        case None =>
      }
    }
    names
  }

  private def loadWordFreq(lang: String): Array[String] ={
    var inputStream: Option[InputStream] = None
    var names: Array[String] = Array[String]()
    try {
      inputStream = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/$lang-wordsfreq.txt"))
      names = scala.io.Source.fromInputStream(inputStream.get).getLines().toArray
    } catch {
      case e: Throwable =>
        System.err.println(s"ERROR: Failed to process ${AppConfig.openNLPModels}/$lang-wordsfreq.txt")
        e.printStackTrace(System.err)
        try{
          inputStream = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/${AppConfig.defaultLanguage}-wordsfreq.txt"))
          names = scala.io.Source.fromInputStream(inputStream.get).getLines().toArray
        }catch {
          case e: Throwable =>
            System.err.println(s"ERROR: Failed to process ${AppConfig.openNLPModels}/$lang-wordsfreq.txt token")
            e.printStackTrace(System.err)
        }
    } finally {
      inputStream match {
        case Some(stream) => stream.close()
        case None =>
      }
    }
    names
  }

  private def loadSWords(lang: String): Array[String] ={
    var inputStream: Option[InputStream] = None
    var names: Array[String] = Array[String]()
    try {
      inputStream = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/$lang-stopwords.txt"))
      names = scala.io.Source.fromInputStream(inputStream.get).getLines().toArray
    } catch {
      case e: Throwable =>
        System.err.println(s"ERROR: Failed to process ${AppConfig.openNLPModels}/$lang-stopwords.txt")
        e.printStackTrace(System.err)
        try{
          inputStream = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/${AppConfig.defaultLanguage}-stopwords.txt"))
          names = scala.io.Source.fromInputStream(inputStream.get).getLines().toArray
        }catch {
          case e: Throwable =>
            System.err.println(s"ERROR: Failed to process ${AppConfig.openNLPModels}/$lang-stopwords.txt token")
            e.printStackTrace(System.err)
        }
    } finally {
      inputStream match {
        case Some(stream) => stream.close()
        case None =>
      }
    }
    names
  }

  def find(text: String): List[(String, String, List[Int])] = {
    var persons: List[(String, String, List[Int])] = (
        clearNames(regex_detector.find(text)).map(x => ("RegexNLP", x))  ++
        clearNames(opennlp_detector.find(text)).map(x => ("OpenNLP", x)) ++
        clearNames(stanford_detector.find(text)).map(x => ("StanfordNLP", x))
      ).groupBy(_._2).map(x => (x._1, x._2.head._1, PreprocessText.findAllPositions(x._1, text))).toList //(person, type detectors, positions)
    persons
  }

  def clearNames(persons: List[String]): List[String] = {
    filterNames(
      persons
        .map(x => c1.replaceAllIn(x, ""))
        .map(x => c2.replaceAllIn(x, ""))
        .filter(x => {
          val n = c3.findAllIn(x).toList
          n.distinct.length > n.length / 2
        })
        .map(x => x.split(' '))
        .filter( x => x.distinct.length == x.length )
        .map(x => {
          x
            .filter(x => !swords.contains(x)) //remove stop words
            .filter(!_.matches(".*\\d+.*")) //remove digits
            .filter(!_.matches("^(M{0,3})(D?C{0,3}|C[DM])(L?X{0,3}|X[LC])(V?I{0,3}|I[VX])$")) //remove Roman numerals
            .filter(x => !x.contains("[") && !x.contains("]") && !x.contains("@")) //remove Roman numerals
        })
        .map(x => x.mkString(" ").trim)
    )
  }

  def filterNames(persons: List[String]): List[String] = {
    val result: List[String] = persons
      .map(x => (x, nounsDetector.getProbability(x, lang)))
      .filter(x => x._2.length > 1)
      .map(x => (x._2.map(y => y._1).mkString(" "), x._2))
      .map(x => {
        (x._1,
          x._2.map(y => (y._1, y._2, PreprocessText.fullClean(y._1, Array(), 0)))
        )
      })
      .map(x => {
        (x._1,
          x._2.map(y => (y._1, y._2, names.contains(y._3), !wordFreq.contains(y._3)))
        )
      })
      .map(x => {
        (x._1,
          x._2.map(y => (y._1, y._2 * 0.4, y._2, if (y._3) 0.5 else 0.0, if (y._4) 0.4 else 0.0))
        )
      })
      .map(x => {
        (x._1,
          x._2.map(y => (y._1, y._2 + y._4 + y._5, y._3, y._4, y._5))
        )
      })
      .map(x => (x._1, x._2.filter(x => x._2 > 0.45 || x._1.length <= 2)))
      .filter(x => x._2.length > 1)
      .map(x => x._2.map(y => y._1).mkString(" "))
      .filter(x => PreprocessText.fullClean(x, Array(), 0).replace(" ", "").length > 2)
      .distinct

    result
  }

  def findNameIdentity(persons: List[(String, String, List[(Int, Int, Int)])]): List[(String, String, List[(Int, Int, Int)])] = {

    def mergePerson(person1: (String, String, List[(Int, Int, Int)]),
                    person2: (String, String, List[(Int, Int, Int)])): (String, String, List[(Int, Int, Int)]) = {

      val ner = if (!person1._2.contains(person2._2)) s"${person1._2} & ${person2._2}" else person1._2
      val name: String = if (person1._1.replaceAll("""[\p{Punct}]""", "").length >
        person2._1.replaceAll("""[\p{Punct}]""", "").length) person1._1 else person2._1
      val positions: List[(Int, Int, Int)] = mergePositions(person1._3, person2._3)

      (name, ner, positions)
    }

    def mergePositions(person1: List[(Int, Int, Int)], person2: List[(Int, Int, Int)]): List[(Int, Int, Int)] = {
      val same: List[(Int, Int, Int)] = person1.intersect(person2)
      val skip: ListBuffer[(Int, Int, Int)] = ListBuffer.empty
      val last: ListBuffer[(Int, Int, Int)] = ListBuffer.empty

      val offset: Int = 10

      val (per1, per2) = (person1.diff(same), person2.diff(same))

      if (per1.isEmpty || per2.isEmpty) if (person1.length > person2.length) return person1 else return person2

      per1.foreach { p1 =>
        per2.diff(per1).filter(!skip.contains(_)).foreach { p2 =>
          if (p1._1 == p2._1 && p1._2 == p2._2) {
            if (math.abs(p1._3 - p2._3) <= offset) {
              last += ((p1._1, p1._2, (p1._3 + p1._3) / 2))
              skip += p1
              skip += p2
            }
          } else if (!skip.contains(p1) && !skip.contains(p2)) {
            last += p1
            skip += p1
            last += p2
            skip += p2
          }
        }
      }
      last.union(same).distinct.toList
    }

    def checkNames(name: String, anotherName: String): Boolean = {
      val n1: Array[String] = name.trim.split(" ")
      val n2: Array[String] = anotherName.trim.split(" ")

      val n3: Array[String] = name.replaceAll("""[\p{Punct}]""", "").split(" ")
      val n4: Array[String] = anotherName.trim.replaceAll("""[\p{Punct}]|»|«""", "").split(" ")

      if (n1.sameElements(n2)) false // the value comparing with itself
      else if (n3 sameElements n4) true // due to Stanford dots in Name
      else if (n1.length == n2.length && n1.length == 2) n1.intersect(n2).length == 2
      else if (n1.length == n2.length && n1.length == 3) n1.intersect(n2).length == 3
      else if (n1.length >= 2 || n2.length >= 2) {
        if (n1.length > n2.length) n1.intersect(n2).length > 1
        else n2.intersect(n1).length > 1
      }
      else false
    }

    var merged: List[(String, String, List[(Int, Int, Int)])] = List()
    var remove: List[(String, String, List[(Int, Int, Int)])] = List()

    persons.foreach { person1 =>
      var personM: Option[(String, String, List[(Int, Int, Int)])] = None
      var toDelete: Boolean = false
      persons.filter(!remove.contains(_))
        .foreach { person2 =>
          if (checkNames(person1._1, person2._1)) {
            personM = Option(mergePerson(personM.getOrElse(person1), person2))
            remove = remove ++ List(person2)
            toDelete = true
          }
        }
      if (toDelete) {
        merged = merged ++ List(mergePerson(personM.get, person1))
        remove = remove ++ List(person1)
      }
    }

    persons.diff(remove).union(merged)
  }
}


