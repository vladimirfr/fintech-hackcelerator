package fintech.hackcelerator.nlp

import fintech.hackcelerator.config.AppConfig
import opennlp.tools.postag.{POSModel, POSTaggerME}
import org.apache.hadoop.fs.{FSDataInputStream, Path}

import scala.collection.mutable.ListBuffer
import scala.util.Try
import scala.util.matching.Regex

class NounsOpenNLPDetector {
  private val defaultModel: String = "en"
  private val models: List[String] = List("de", "en", "fr")
  private var posModel: POSModel = null
  private val opennlp_resources: String = AppConfig.openNLPModels

  def this(lang: String) = {
    this()
    init(lang)
  }

  val nounsMap: Map[String, List[String]] = Map[String, List[String]](
    "en" -> List("NN", "NNS", "NNP", "NNPS"),
    "de" -> List("NN", "NE", "NNS", "NNP", "NNPS"),
    "fr" -> List("NC", "NP", "NN", "NNS", "NNP", "NNPS")
  )

  def getProbability(in_data: String, lng: String): List[(String, Double)] = {
    var lang: String = lng
    val additional_punct: Regex = new Regex("[^A-Za-z ]+")
    var tokens: Array[String] = in_data.split(' ').filter(!_.isEmpty).map(_.capitalize)
    var tokens_lower: Array[String] = in_data.split(' ').filter(!_.isEmpty).map(_.toLowerCase)
    if (posModel == null) {
      println("empty models!!!")
      return tokens.map(x => (x, 0.0)).toList
    }
    try {
      //var nouns: String = ""
      var nouns_buf: ListBuffer[(String, Double)] = new ListBuffer[(String, Double)]()
      val posTagger: POSTaggerME = new POSTaggerME(posModel)

      val posTags: Array[String] = posTagger.tag(tokens)
      val posTags_lower: Array[String] = posTagger.tag(tokens_lower)
      val probs = posTagger.probs()
      val probs_lower = posTagger.probs()
      val enabled_nouns: List[String] = List("NNP", "NNPS")
      val enabled_nouns_lower: List[String] = List("NN", "NNP")
      for (i <- posTags.indices) {
        val tag: String = posTags(i)
        val tag_lower: String = posTags_lower(i)
        println(f"token: ${tokens(i)}; postag: ${posTags(i)} ==> prob = ${probs(i)}")
        if (enabled_nouns.contains(tag)) {
          if (enabled_nouns_lower.contains(tag_lower)) {
            nouns_buf += ((tokens(i), (probs(i) + probs_lower(i)) / 2.0))
          } else {
            nouns_buf += ((tokens(i), probs(i) * 0.5))
          }
          //println(nouns_buf.last)
        }
      }
      return nouns_buf.toList
    }
    catch {
      case e: Exception =>
        println("Missed nouns extraction")
        e.printStackTrace(System.err)
        return tokens.map(x => (x, 0.0)).toList
    }
  }


  def init(lang: String): Unit = {
    val fs: org.apache.hadoop.fs.FileSystem = org.apache.hadoop.fs.FileSystem.get(AppConfig.getConfig())
    try {
      // POS-tagger
      var maxEntPath: Path = new Path(s"$opennlp_resources/$lang-pos-maxent.bin")
      maxEntPath = if (fs.exists(maxEntPath)) maxEntPath else new Path(s"$opennlp_resources/$defaultModel-pos-maxent.bin")
      if (fs.exists(maxEntPath)) {
        val maxEntStream: FSDataInputStream = fs.open(maxEntPath)
        if (maxEntStream != null) {
          val posModeler: POSModel = new POSModel(maxEntStream)
          posModel = posModeler
          Try(maxEntStream.close())
        }
        else {
          println(s"Nouns: Failed to load $lang")
        }
      }
    } catch {
      case e: Throwable =>
        System.err.println(s"Failed to process $lang")
        e.printStackTrace(System.err)
    }
  }
}
