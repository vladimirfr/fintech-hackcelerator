package fintech.hackcelerator.nlp

import java.io.InputStream

import fintech.hackcelerator.config.AppConfig
import fintech.hackcelerator.tools.Resources
import opennlp.tools.namefind.{NameFinderME, TokenNameFinderModel}
import opennlp.tools.tokenize.{TokenizerME, TokenizerModel}

class OpenNLP(lang: String) {
  private lazy val tokenizer: TokenizerME = buildTokenizerME
  private lazy val personME: NameFinderME = buildNameFinderME(s"${lang}_ner_person.bin", s"${AppConfig.defaultLanguage}_ner_person.bin")

  def find(text: String): List[String] = {
    find(personME, text)
  }

  def find(finder: NameFinderME, text: String): List[String] = {
    val words: Array[String] = tokenizer.tokenize(text)
    finder.find(words).map(span => {
      val start: Int = span.getStart
      val end: Int = span.getEnd
      (words.slice(start, end).mkString(" "))
    }).toList.distinct
  }

  /** Function to build TokenizerME for class input locale or default en [[tokenizer]] */
  def buildTokenizerME: TokenizerME = {
    var inputStream: Option[InputStream] = None
    var tokenizerME: Option[TokenizerME] = None
    try {
      inputStream = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/$lang-token.bin"))
      tokenizerME = Option(new TokenizerME(new TokenizerModel(inputStream.get)))
    } catch {
      case e: Throwable =>
        System.err.println(s"ERROR: Failed to process ${AppConfig.openNLPModels}/$lang-token.bin token")
        e.printStackTrace(System.err)
        try{
          inputStream = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/${AppConfig.defaultLanguage}-token.bin"))
          tokenizerME = Option(new TokenizerME(new TokenizerModel(inputStream.get)))
        }catch {
          case e: Throwable =>
            System.err.println(s"ERROR: Failed to process ${AppConfig.openNLPModels}/$lang-token.bin token")
            e.printStackTrace(System.err)
        }
    } finally {
      inputStream match {
        case Some(stream) => stream.close()
        case None =>
      }
    }
    tokenizerME.get
  }

  /** Function to build entity finder @see reorganized due to exceptions and local resources */
  def buildNameFinderME(model: String, default: String): NameFinderME = {
    var modelIn: Option[InputStream] = None
    var nameFinderME: Option[NameFinderME] = None
    try {
      modelIn = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/$model"))
      nameFinderME = Option(new NameFinderME(new TokenNameFinderModel(modelIn.get)))
    } catch {
      case e: Throwable =>
        System.err.println(s"Failed load $model model from resources, try to load default model from resources")
        e.printStackTrace(System.err)
        try {
          modelIn = Option(Resources.getResourceAsStream(s"${AppConfig.openNLPModels}/$default"))
          nameFinderME = Option(new NameFinderME(new TokenNameFinderModel(modelIn.get)))
        } catch {
          case e: Throwable =>
            System.err.println(s"Failed load default $model model from resources: ${e.getMessage}")
            e.printStackTrace(System.err)
            throw new RuntimeException(e)
        }
    } finally {
      modelIn match {
        case Some(stream) => stream.close()
        case None =>
      }
    }
    nameFinderME.get
  }

}
