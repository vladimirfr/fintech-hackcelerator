package fintech.hackcelerator.nlp

import fintech.hackcelerator.config.AppConfig

import scala.util.matching.Regex

class RegexNLP(lang: String) {
  var regexs: Map[String, List[Regex]] = Map(
    "en" -> List(
      new Regex("[^A-Za-z0-9\\-]((?:[A-Z]\\. ?)?([A-Z]\\. *|[A-Z][a-z]{2,} *)+ *([A-Z][a-z]{2,}|[A-Z])(?: +[A-Z][a-z]{2,}){0,3}|[A-Z][a-z]+,+( *[A-Z]+\\.){2,2})[^A-Za-z0-9\\-]"), //,
      new Regex("[^A-Za-z0-9\\-]((?i)[^a-z](mrs|m|mr|master|esquire|sir|sire|lord|miss|ms|mistress|madam|dame|lady)[. ]+)([A-Z](\\.|[a-z]+) *){1,3}[^A-Za-z0-9\\-]")
    ),
    "fr" -> List(
      new Regex("(?:[A-Z]\\. ?)?[A-Z][a-z]+[. ]+[A-Z][a-z]+(?: +[A-Z][a-z]+)?"),
      new Regex("[A-Z][a-z]+,+( *[A-Z]+\\.)+"),
      new Regex("[A-Z][a-z]{3,}( |\\-)*[A-Z][a-z]{3,}( |\\-)*[A-Z][a-z]{3,}"),
      new Regex("[A-Z][a-z]+ +((?i)(de +la|de|van|le) +)+[A-Z][a-z]+"),
      new Regex("((?i)(sra|m|mr|mme|mlle|mm|mmes|mlles)[. ]+)([A-Z](\\.|[a-z]+) *){1,3}")
    ),
    "de" -> List(
      new Regex("(?:[A-Z]\\. ?)?[A-Z][a-z]+[. ]+[A-Z][a-z]+(?: +[A-Z][a-z]+)?"),
      new Regex("[A-Z][a-z]+,+( *[A-Z]+\\.)+"),
      new Regex("[A-Z][a-z]+( *[A-Z]+(?:\\:|\\-[A-Z]:))+"),
      new Regex("[A-Z][a-z]{3,}( |\\-)*[A-Z][a-z]{3,}( |\\-)*[A-Z][a-z]{3,}"),
      new Regex("([A-Z]([a-z]+|\\.) *)+((?i)( *von *))?([A-Z]([a-z]+|\\.) *)+"),
      new Regex("((?i)((herr[a-z]*|hr|Machthaber|Staatsoberhaupt|frau[a-z]|Fr|Dame|Weib|Gattin|Ehefrau)[. ]+))([A-Z](\\.|[a-z]+) *){1,3}")
    ),
    "it" -> List(
      new Regex("(?:[A-Z]\\. ?)?[A-Z][a-z]+[. ]+[A-Z][a-z]+(?: +[A-Z][a-z]+)?"),
      new Regex("[A-Z][a-z]+,+( *[A-Z]+\\.)+")
    )
  )

  def find(text: String): List[String] = {
    var persons: List[String] = List()
    var finalPersons: List[(String, String, List[(Int, Int, Int)])] = List()

    for (regex: Regex <- regexs.getOrElse(lang, regexs(AppConfig.defaultLanguage))) {
      persons = persons ++ regex.findAllIn(s" $text ").toList
    }

    persons.distinct
  }
}
