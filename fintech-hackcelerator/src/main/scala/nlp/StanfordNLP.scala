package fintech.hackcelerator.nlp

import java.util.Properties

import edu.stanford.nlp.ling.CoreAnnotations.{SentencesAnnotation, TokensAnnotation}
import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}
import edu.stanford.nlp.util.StringUtils

import scala.collection.JavaConversions._

class StanfordNLP(language: String) {
  var lang: String = _

  language match {
    case "de" => lang = "-german"
    case "fr" => lang = "-french"
    case _ => lang = "" // english property file name is StanfordCoreNLP.properties without hyphen
  }

  val props: Properties = StringUtils.argsToProperties(s"-props StanfordCoreNLP$lang.properties")
  props("annotators") = "tokenize, ssplit, pos, lemma, ner" // TODO: add static preconfigured resources
  lazy val pipeline: StanfordCoreNLP = new StanfordCoreNLP(props)

  def find(text: String): List[(String)] = { // as in OpenNLPNER
    val sent: Annotation = annotate(text)
      sent.get(classOf[SentencesAnnotation])
        .flatMap(sentence => sentence.get(classOf[TokensAnnotation]).map(coreLabel => (coreLabel.value(), coreLabel.ner())))
        .map(x => if (x._2 == "PERSON") x._1 + " " else "|")
        .mkString.split("""\|""").filter(!_.isEmpty).map(p => p).toList.distinct
  }

  def getSentences(text: String): Array[String] = {
    val sent: Annotation = annotate(text)
    sent.get(classOf[SentencesAnnotation]).map(_.toString).toArray
  }

  private def annotate(text: String): Annotation = {
    val annotation: Annotation = new Annotation(text)
    pipeline.annotate(annotation)
    annotation
  }
}
