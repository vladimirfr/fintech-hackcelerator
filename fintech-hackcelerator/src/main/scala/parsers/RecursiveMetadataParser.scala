package fintech.hackcelerator.parsers

import java.io.InputStream

import org.apache.tika.metadata.Metadata
import org.apache.tika.parser.{ParseContext, Parser, ParserDecorator}
import org.xml.sax.ContentHandler

//Wrapper to extract the content from archive file recursively
class RecursiveMetadataParser(parser: Parser) extends ParserDecorator(parser) {
  override def parse(stream: InputStream,
                     ignore: ContentHandler,
                     metadata: Metadata,
                     context: ParseContext) {
    super.parse(stream, ignore, metadata, context)
  }
}
