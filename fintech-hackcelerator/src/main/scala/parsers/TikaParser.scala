package fintech.hackcelerator.parsers

import java.io.{File, FileInputStream, InputStream}
import java.util

import fintech.hackcelerator.config.AppConfig
import fintech.hackcelerator.tools.{DocTypeDetector, LogHelper}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.tika.config.{ServiceLoader, TikaConfig}
import org.apache.tika.detect
import org.apache.tika.detect.{CompositeDetector, DefaultDetector, Detector}
import org.apache.tika.exception.{AccessPermissionException, EncryptedDocumentException, TikaException}
import org.apache.tika.io.TikaInputStream
import org.apache.tika.langdetect.OptimaizeLangDetector
import org.apache.tika.language.detect.{LanguageDetector, LanguageResult}
import org.apache.tika.metadata.Metadata
import org.apache.tika.mime.MimeTypes
import org.apache.tika.parser.microsoft.{OfficeParserConfig, POIFSContainerDetector}
import org.apache.tika.parser.ocr.TesseractOCRConfig
import org.apache.tika.parser.pdf.PDFParserConfig
import org.apache.tika.parser.pkg.ZipContainerDetector
import org.apache.tika.parser.{AutoDetectParser, ParseContext, Parser}
import org.apache.tika.sax.BodyContentHandler

import scala.collection.JavaConversions._
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.Try

object TikaParser extends LogHelper with java.io.Serializable {

  private def getExtension(name: String): String = {
    name.substring(name.lastIndexOf(".") + 1).toLowerCase
  }

  private def openFile(file: String): InputStream = {
    var is: InputStream = null
    var file_exist: Boolean = false
    val fs = FileSystem.get(AppConfig.getConfig())
    val hdfsPath = new Path(file)

    try {
      file_exist = fs.exists(hdfsPath)
    }
    catch {
      case e: Throwable =>
        logger.error("Can't check if file exists")
        e.printStackTrace(System.err)
    }

    if (file_exist) {
      is = fs.open(hdfsPath)
      logger.info(s"Reading $file from HDFS")
    }
    else {
      is = new FileInputStream(new File(file))
      logger.info(s"Reading $file from local")
    }
    is
  }

  private def getParser(ext: String): Parser = {
    var p: Parser = null

    val sl: ServiceLoader = new ServiceLoader()
    val mt: MimeTypes = TikaConfig.getDefaultConfig.getMimeRepository
    val t: util.List[Detector] = new DefaultDetector(mt, sl).getDetectors
    val ld: List[Detector] = new DefaultDetector(mt, sl).getDetectors.toList
      .filter(d => d.getClass != classOf[ZipContainerDetector])
      .filter(d => d.getClass != classOf[POIFSContainerDetector])

    val cd: CompositeDetector = new detect.CompositeDetector(ld)

    ext match {
      case "zip" | "rar" => p = new RecursiveMetadataParser(new AutoDetectParser(cd))
      case _ => p = new AutoDetectParser(cd)
    }
    p
  }

  private def getContext(ext: String, p: Parser): ParseContext = {
    val context: ParseContext = new ParseContext()
    if (ext.contains("pdf")) {
      val config: TesseractOCRConfig = new TesseractOCRConfig()
      val pdfConfig: PDFParserConfig = new PDFParserConfig()

      val ocr_mode: String = AppConfig.ocrMode

      if (!ocr_mode.contains("no_ocr")) {
        pdfConfig.setExtractInlineImages(true)
        pdfConfig.setOcrImageType("rgb")
        pdfConfig.setOcrDPI(100)
      }
      else {
        pdfConfig.setExtractInlineImages(false)
      }
      pdfConfig.setOcrStrategy(ocr_mode)

      context.set(classOf[TesseractOCRConfig], config)
      context.set(classOf[PDFParserConfig], pdfConfig)
      //context.set(classOf[Parser], p)
    }

    if (ext.contains("xls") || ext.contains("doc") || ext.contains("ppt")) {
      val config: OfficeParserConfig = new OfficeParserConfig()
      config.setUseSAXDocxExtractor(true)
      config.setUseSAXPptxExtractor(true)
      config.setIncludeHeadersAndFooters(true)
      context.set(classOf[OfficeParserConfig], config)
      context.set(classOf[Parser], p)
    }
    context
  }

  def getLanguage(content: String): String = {
    var language = "other"
    try {
      val detector: LanguageDetector = new OptimaizeLangDetector().loadModels()
      val result: LanguageResult = detector.detect(content)
      language = result.getLanguage
    } catch {
      case ex: Throwable =>
        logger.error("lang detector error")
        logger.error(ex.getMessage)
        logger.error(ex.getStackTraceString)
    }
    language
  }

  def extractInfo(fp: String, fs: FileSystem): (String, String, Metadata, String, String, Boolean, String, String) = {
    val ext: String = getExtension(fp)
    val p: Parser = getParser(ext)

    var has_exception: String = ""
    var content: String = ""
    var encrypted: Boolean = false
    var language: String = ""

    var is: InputStream = openFile(fp)

    val handler: BodyContentHandler = if (ext.contains("htm")) new WhiteSpaceHandler(-1) else new BodyContentHandler(-1)
    val metadata: Metadata = new Metadata()
    val context: ParseContext = getContext(ext, p)

    try {
      val tis: TikaInputStream = TikaInputStream.get(is)
      p.parse(tis, handler, metadata, context)

      content = handler.toString
      //check ocr mode

      language = getLanguage(content)
    }
    catch {
      case ex@(_: EncryptedDocumentException | _: org.apache.poi.EncryptedDocumentException) =>
        encrypted = true
        has_exception = "Encrypted"
        logger.error(s"Document is encrypted: $fp")
        logger.error(ex.getMessage)
        logger.error(ex.getStackTraceString)
      case ex: AccessPermissionException =>
        logger.error("TikaException, please check the file: " + fp + "\n")
        has_exception = "Access denied"
        logger.error(ex.getMessage)
        logger.error(ex.getStackTraceString)
      case ex: TikaException =>
        logger.error("TikaException, please check the file: " + fp + "\n")
        logger.error(ex.getMessage)
        content = handler.toString
        if (content.length > 0) {
          language = getLanguage(content)

          has_exception = ""
        }
        else {
          has_exception = "TikaException"
          logger.error(ex.getStackTraceString)
        }

      case ex: Throwable =>
        has_exception = ex.getMessage
        logger.error(s"Exception, please check the file: $fp")
        ex.printStackTrace(System.err)
    }
    finally {
      is.close()
    }

    (ext, content, metadata, language, has_exception, encrypted, DocTypeDetector.detect(content, ext), DocTypeDetector.detectTextType(content, ext))
  }

  def extract(filePath: String, fs: FileSystem): Map[String, Any] = {
    val (fileext, content, metadata, language, has_exception, encrypted, docType, textDocType) = Try(extractInfo(filePath, fs))
      .getOrElse(("", "", new Metadata(), "", "true", false, "", "Empty"))

    var lastmodified: String = ""
    var creationdate: String = ""
    var creator: String = ""
    var lastmodifier: String = ""

    if (metadata.get("Last-Modified") != null) {
      lastmodified = metadata.get("Last-Modified")
    }
    if (metadata.get("Creation-Date") != null) {
      creationdate = metadata.get("Creation-Date")
    }
    if (metadata.get("creator") != null) {
      creator = metadata.get("creator")
    }
    if (metadata.get("Last-Author") != null) {
      lastmodifier = metadata.get("Last-Author")
    }

    Map("file" -> filePath,
      "has_exception" -> has_exception,
      "encrypted" -> encrypted,
      "lastmodified" -> lastmodified.toLowerCase,
      "creationdate" -> creationdate.toLowerCase,
      "extension" -> fileext.toLowerCase,
      "content" -> content,
      "metadata" -> metadata.toString.toLowerCase,
      "docType" -> docType,
      "language" -> language.toLowerCase,
      "creator" -> creator.toLowerCase,
      "textDocType" -> textDocType,
      "lastmodifier" -> lastmodifier.toLowerCase)
  }

  def extractFuture(file: String, fs: FileSystem): Map[String, Any] = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global

    val file_name: String = AppConfig.dataset + file
    val file_ext = file_name.substring(file_name.lastIndexOf(".") + 1).toLowerCase
    var metadata: Map[String, Any] = Map(
      "file" -> file_name,
      "has_exception" -> "",
      "encrypted" -> false,
      "lastmodified" -> "",
      "creationdate" -> "",
      "extension" -> file_ext,
      "docType" -> DocTypeDetector.matchDocType(file_ext),
      "content" -> "",
      "metadata" -> "",
      "language" -> "",
      "creator" -> "",
      "ner" -> Map[(String, String), String](),
      "lastmodifier" -> "",
      "textDocType" -> "Empty"
    )

    val extractFuture: Future[Map[String, Any]] = Future {
      val meta: Map[String, Any] = extract(file, fs)
      meta
    }

    try {
      metadata = Await.result(extractFuture, AppConfig.maxTime seconds)
    }
    catch {
      case e: Throwable =>
        println(s"Can't extract file $file_name")
        e.printStackTrace(System.out)
    }
    metadata
  }
}
