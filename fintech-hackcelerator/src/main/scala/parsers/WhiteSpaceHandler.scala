package fintech.hackcelerator.parsers

import org.apache.tika.sax.BodyContentHandler

class WhiteSpaceHandler(limit: Int) extends BodyContentHandler(limit) {
  override def characters(ch: Array[Char], start: Int, length: Int) {
    super.characters(Array(' ') ++ ch, start, length + 1)
  }
}
