package fintech.hackcelerator.tools

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import fintech.hackcelerator.config.AppConfig
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.storage.StorageLevel._

object AppSpark {
  var ctx: SparkContext = null
  val config: Config = ConfigFactory.parseFile(new File("Dathena.conf"))
  val hostname: String = config.getString("hostname")

  val storageLevel: Map[String, StorageLevel] = Map(
    "DISK_ONLY" -> DISK_ONLY,
    "MEMORY_AND_DISK" -> MEMORY_AND_DISK,
    "MEMORY_AND_DISK_SER" -> MEMORY_AND_DISK_SER,
    "MEMORY_ONLY" -> MEMORY_ONLY,
    "MEMORY_ONLY_SER" -> MEMORY_ONLY_SER
  )
  val cacheLevel = storageLevel.getOrElse(AppConfig.cache_level, MEMORY_ONLY)

  val partitions: Int = config.getString("partitions").toInt
  var appName: String = "Spark HDFS Tika"
  val executors: Int = config.getString("num-executors").toInt

  def getContext(): SparkContext = {
    // https://spark.apache.org/docs/latest/configuration.html
    val conf: SparkConf = new SparkConf()
      .setMaster(config.getString("master"))
      .setAppName(appName)
      .set("spark.submit.deployMode", AppConfig.deploy_mode)
      .set("spark.executor.instances", AppConfig.num_executors)
      .set("spark.driver.memory", AppConfig.driver_memory)
      .set("spark.executor.memory", AppConfig.executor_memory)
      .set("spark.executor.cores", AppConfig.executor_cores)
      .set("spark.yarn.am.cores", AppConfig.spark_yarn_am_cores)
      .set("spark.serializer", AppConfig.spark_serializer)
      .set("spark.driver.maxResultSize", "10g")
      .set("spark.kryoserializer.buffer.max.mb", "512MB")

    ctx = SparkContext.getOrCreate(conf)
    ctx
  }
}
