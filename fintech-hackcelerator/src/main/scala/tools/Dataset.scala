package fintech.hackcelerator.tools

import java.io.File

import fintech.hackcelerator.config.AppConfig
import org.apache.hadoop.conf.Configuration
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object Dataset {
  // TODO: https://stackoverflow.com/questions/1844688/read-all-files-in-a-folder
  def recursiveListFiles(f: File, depth: Int = 5): Array[File] = {
    if (!f.isDirectory) {
      return Array[File]()
    }

    val these: Array[File] = f.listFiles

    if (depth != 0) {
      try {
        these ++ these.filter(_.isDirectory).flatMap(x => recursiveListFiles(x, Math.max(-1, depth - 1)))
      }
      catch {
        case _: Exception => these
      }
    }
    else {
      these
    }
  }

  def getAllFilesList(sc: SparkContext, path: String): RDD[String] = {
    val conf: Configuration = sc.hadoopConfiguration
    val fs: org.apache.hadoop.fs.FileSystem = org.apache.hadoop.fs.FileSystem.get(conf)
    if (fs.exists(new org.apache.hadoop.fs.Path(AppConfig.file_listing_path))) {
      sc.wholeTextFiles(AppConfig.file_listing_path, AppSpark.partitions)
        .flatMap(_._2.split("\n"))
        .repartition(AppSpark.partitions)
    }else{
      val dir: File = new File(path)
      val files: Array[File] = recursiveListFiles(dir)
      val files_rdd: RDD[String] = sc.parallelize(files, AppConfig.partitions).map(x => x.getPath)
      files_rdd.saveAsTextFile(AppConfig.file_listing_path)
      files_rdd
    }
  }

  def randomSplit(files: RDD[String]): Array[RDD[String]] = {
    val sections: Int = (files.count() / AppConfig.section_batch).toInt
    val weights: Array[Double] = Array.fill(sections)(1.toDouble / sections)
    val items: Array[RDD[String]] = files.randomSplit(weights)
    items
  }

  def getFilesBySections(sc: SparkContext, path: String):Array[RDD[String]] = {
    randomSplit(getAllFilesList(sc,path))
  }
}
