package fintech.hackcelerator.tools

object DocTypeDetector {
  val emails_head_keywords = List("from", "sent", "subject")
  val emails_max_sentences_distace = 20
  val emails_max_distace = 1000

  def checkEmail(text: String): Boolean = {
    val emails_head_keywords_index = emails_head_keywords.zipWithIndex

    //get first senteces
    val sentences: Array[(String, Int)] = text
      .slice(0, emails_max_distace)
      .split("(( *\\n)+)")
      .map(x => x.split(':').head.toLowerCase)
      .zipWithIndex
      .filter(x => x._2 <= emails_max_sentences_distace)

    if (sentences.length < 3) return false

    //get exists keywords
    val keywords: Array[(Int, Int)] = sentences
      .map(x => {
        emails_head_keywords_index.map(y => (x._1.contains(y._1), y._2, x._2)).filter(y => y._1)
      })
      .filter(x => x.nonEmpty)
      .map(x => x.head)
      .map(x => (x._2, x._3))

    if (keywords.length < 2) return false

    //check emails
    for (i <- 0 until keywords.length - 1) {
      val keyword_1 = keywords(i)
      val keyword_2 = keywords(i + 1)
      if (keyword_2._1 > keyword_1._1 && (keyword_2._2 - keyword_1._2) <= 3 && (keyword_2._2 - keyword_1._2) > 0) {
        return true
      }
    }
    false
  }

  def checkTable(text: String): Double = {
    val paragraphs: Array[Int] = text.split("((\\n *\\n)+)").flatMap(x => x.split("\\n[A-Z0-9]")).map(x => x.split("\\n").length)
    var tables: Int = 0
    for (i <- 0 until paragraphs.length - 2) {
      if (paragraphs(i) < 2 && paragraphs(i + 1) < 2 && paragraphs(i + 2) < 2) {
        tables += 1
      }
    }
    if (tables == 0) return 0.0
    tables / paragraphs.length.toDouble
  }

  def detectTextType(text: String, ext: String): String  = {
    if(text.length <3) {
      return matchTextType("empty")
    }else if(List("xls", "xlsx", "xlsm").contains(ext.toLowerCase)){
      return matchTextType("table")
    }else if(checkEmail(text)){
      return matchDocType("email")
    }else if(checkTable(text) > 0.7){
      return matchTextType("table")
    }else{
      return matchTextType("text")
    }
  }

  def detect(text:String, extension: String): String ={
    if( checkEmail(text))  return matchDocType("email")
    return matchDocType(extension, true)
  }

  def matchTextType(x: String): String = x match {
    case "email" => "Email"
    case "table" => "Table"
    case "text" => "PlainText"
    case _ => "Empty"
  }

  def ParagraphTypeDetect(text: String): String = {
    val sents: Array[String] = text.split("\n+ *(?=[A-Z0-9])")
    val sent_length: Array[Int] = sents.map(x => x.split(" ").length)
    val avg: Float = sent_length.sum.toFloat / sents.length.toFloat
    val sents_diff: Array[Float] = sent_length.map(x => Math.abs(x - avg))
    var t = List[Float]()
    for (i <- 0 until sents_diff.length - 1) {
      t = t :+ Math.abs(sents_diff(i) - sents_diff(i + 1))
    }
    val p = t.sum / t.length.toFloat
    if (p < 2 && sents.length > 2) "Table"
    else "Text"
  }

  def matchDocTypeByExt(x: String): String = x match {
    case "doc" => "Word"
    case "docx" => "Word"
    case "rtf" => "Word"
    case "pdf" => "PDF"
    case "xls" => "Excel"
    case "xlsx" => "Excel"
    case "xlsm" => "Excel"
    case "ppt" => "Power Point"
    case "pptx" => "Power Point"
    case _ => "Others"
  }

  def matchDocType(x: String, ext_type: Boolean = false): String = {
    if(ext_type){
      return matchDocTypeByExt(x)
    } else {
      return x match {
        case "email" => "Email"
        case _ => matchDocTypeByExt(x)
      }
    }
  }
}
