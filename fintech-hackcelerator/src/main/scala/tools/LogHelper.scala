package fintech.hackcelerator.tools

import org.apache.log4j.Logger

trait LogHelper {
  lazy val logger: Logger = Logger.getLogger("FHLogger")
}
