package fintech.hackcelerator.tools

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.util.matching.Regex

object PreprocessText {
  lazy val diacr: Map[Char, Char] = Map('À' -> 'a', 'Á' -> 'a', 'Â' -> 'a', 'Ã' -> 'a', 'Ä' -> 'a', 'Å' -> 'a', 'Æ' -> 'a', 'Ç' -> 'c', 'È' -> 'e', 'É' -> 'e', 'Ê' -> 'e', 'Ë' -> 'e', 'Ì' -> 'i', 'Í' -> 'i', 'Î' -> 'i', 'Ï' -> 'i', 'Ð' -> 'e', 'Ñ' -> 'n', 'Ò' -> 'o', 'Ó' -> 'o', 'Ô' -> 'o', 'Õ' -> 'o', 'Ö' -> 'o', 'Ø' -> 'o', 'Ù' -> 'u', 'Ú' -> 'u', 'Û' -> 'u', 'Ü' -> 'u', 'Ý' -> 'y', 'Þ' -> 'p', 'ß' -> 's', 'à' -> 'a', 'á' -> 'a', 'â' -> 'a', 'ã' -> 'a', 'ä' -> 'a', 'å' -> 'a', 'æ' -> 'a', 'ç' -> 'c', 'è' -> 'e', 'é' -> 'e', 'ê' -> 'e', 'ë' -> 'e', 'ì' -> 'i', 'í' -> 'i', 'î' -> 'i', 'ï' -> 'i', 'ð' -> 'e', 'ñ' -> 'n', 'ò' -> 'o', 'ó' -> 'o', 'ô' -> 'o', 'õ' -> 'o', 'ö' -> 'o', 'ø' -> 'o', 'ù' -> 'u', 'ú' -> 'u', 'û' -> 'u', 'ü' -> 'u', 'ý' -> 'y', 'þ' -> 'p', 'ÿ' -> 'y', 'Ā' -> 'a', 'ā' -> 'a', 'Ă' -> 'a', 'ă' -> 'a', 'Ą' -> 'a', 'ą' -> 'a', 'Ć' -> 'c', 'ć' -> 'c', 'Ĉ' -> 'c', 'ĉ' -> 'c', 'Ċ' -> 'c', 'ċ' -> 'c', 'Č' -> 'c', 'č' -> 'c', 'Ď' -> 'd', 'ď' -> 'd', 'Đ' -> 'd', 'đ' -> 'd', 'Ē' -> 'e', 'ē' -> 'e', 'Ĕ' -> 'e', 'ĕ' -> 'e', 'Ė' -> 'e', 'ė' -> 'e', 'Ę' -> 'e', 'ę' -> 'e', 'Ě' -> 'e', 'ě' -> 'e', 'Ĝ' -> 'g', 'ĝ' -> 'g', 'Ğ' -> 'g', 'ğ' -> 'g', 'Ġ' -> 'g', 'ġ' -> 'g', 'Ģ' -> 'g', 'ģ' -> 'g', 'Ĥ' -> 'h', 'ĥ' -> 'h', 'Ħ' -> 'h', 'ħ' -> 'h', 'Ĩ' -> 'i', 'ĩ' -> 'i', 'Ī' -> 'i', 'ī' -> 'i', 'Ĭ' -> 'i', 'ĭ' -> 'i', 'Į' -> 'i', 'į' -> 'i', 'İ' -> 'i', 'ı' -> 'i', 'Ĳ' -> 'i', 'ĳ' -> 'i', 'Ĵ' -> 'j', 'ĵ' -> 'j', 'Ķ' -> 'k', 'ķ' -> 'k', 'ĸ' -> 'k', 'Ĺ' -> 'l', 'ĺ' -> 'l', 'Ļ' -> 'l', 'ļ' -> 'l', 'Ľ' -> 'l', 'ľ' -> 'l', 'Ŀ' -> 'l', 'ŀ' -> 'l', 'Ł' -> 'l', 'ł' -> 'l', 'Ń' -> 'n', 'ń' -> 'n', 'Ņ' -> 'n', 'ņ' -> 'n', 'Ň' -> 'n', 'ň' -> 'n', 'ŉ' -> 'n', 'Ŋ' -> 'n', 'ŋ' -> 'n', 'Ō' -> 'o', 'ō' -> 'o', 'Ŏ' -> 'o', 'ŏ' -> 'o', 'Ő' -> 'o', 'ő' -> 'o', 'Œ' -> 'o', 'œ' -> 'o', 'Ŕ' -> 'r', 'ŕ' -> 'r', 'Ŗ' -> 'r', 'ŗ' -> 'r', 'Ř' -> 'r', 'ř' -> 'r', 'Ś' -> 's', 'ś' -> 's', 'Ŝ' -> 's', 'ŝ' -> 's', 'Ş' -> 's', 'ş' -> 's', 'Š' -> 's', 'š' -> 's', 'Ţ' -> 't', 'ţ' -> 't', 'Ť' -> 't', 'ť' -> 't', 'Ŧ' -> 't', 'ŧ' -> 't', 'Ũ' -> 'u', 'ũ' -> 'u', 'Ū' -> 'u', 'ū' -> 'u', 'Ŭ' -> 'u', 'ŭ' -> 'u', 'Ů' -> 'u', 'ů' -> 'u', 'Ű' -> 'u', 'ű' -> 'u', 'Ų' -> 'u', 'ų' -> 'u', 'Ŵ' -> 'w', 'ŵ' -> 'w', 'Ŷ' -> 'y', 'ŷ' -> 'y', 'Ÿ' -> 'y', 'Ź' -> 'z', 'ź' -> 'z', 'Ż' -> 'z', 'ż' -> 'z', 'Ž' -> 'z', 'ž' -> 'z', 'ſ' -> 's')
  lazy val sentences_regex: Regex = """(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s""".r
  lazy val words_regex: Regex = """[ \p{Punct}\s]+""".r
  lazy val nlb: Regex = new Regex("[-]+[^A-Za-z0-9]+") //word break

  def findAllPositions(word: String, text: String): List[Int] = {
    val result:ListBuffer[Int] = ListBuffer()
    val words: Array[String] = words_regex.split(word).filter(_.nonEmpty)
    val texts: Array[String] = words_regex.split(text).filter(_.nonEmpty)
    val tl: Int = texts.length
    val wl: Int = words.length
    if(tl < wl) return List()
    var i = 0
    while(i < tl - wl){
      val finded = texts.slice(i,i + wl)
      if(finded.deep == words.deep) {
        println(i, finded.mkString(" "))
        result.append(i)
      }
      i += 1
    }
    result.toList
  }

  def replaceNewLinesAndSpaces(text: String): String = text.replaceAll("""\n+|\s+""", " ")

  def clean(text: String): String = {
    val data: String = nlb.replaceAllIn(text.trim.replace("�", " "), "")
      .replaceAll("-\n ", "").replaceAll("- \n", "").replaceAll("- \n ", "").replaceAll("-\n", "") //word break
    data
  }

  def fullClean(text: String, stopwords: Array[String], min_length: Int = 3): String = {
    val num: Regex = new Regex("[0-9]+")
    val punct: Regex = new Regex("[\\p{Punct}\\s]+")
    val additional_punct: Regex = new Regex("[^A-Za-z ]+")
    val nl: Regex = new Regex("\\n")
    val articles: Regex = new Regex("[^A-Za-z][A-Za-z]{1,3}'")

    var data: String = text.trim.toLowerCase

    data = nl.replaceAllIn(punct.replaceAllIn(articles.replaceAllIn(num.replaceAllIn(data, ""), " "), " "), " ")

    data = additional_punct.replaceAllIn(data, "")
      .split(" ").filter(x => x.length >  min_length && x.length < 20)
      .filter(_.nonEmpty).map(x => ConvertDiacritic(x))
      .filter(!_.matches(".*[^aeioyu]{4,}.*")).filter(!stopwords.contains(_)).mkString(" ")

    return data
  }

  def getParagraphs(text: String): Array[String] = {
    val paragraphs: Array[String] = clean(text).split("""(?m)(?=^((\n\r)|(\r\n)){2,}|((\r)|(\n)){2,})""").map(_.trim)
    val avgCountLines: Float = paragraphs.map(x => x.split("""\n""").length).sum / paragraphs.length.toFloat
    if (avgCountLines < 3) clean(text).split("""(?m)(?=^((\n\r)|(\r\n)){3}|((\r)|(\n)){3})""").map(_.trim)
    else paragraphs
  }

  def getSentences(text: String): Array[String] = {
    sentences_regex.split(text)
  }

  def getSentencesWithDist(text: String): Array[(Int, Int, String)] = {
    val sentences: Array[(Int, String)] = sentences_regex.split(text).filter(_.nonEmpty).map(x => (words_regex.split(x).length, x))
    sentences(0) = (0, sentences(0)._2)
    for(i <- 1 until sentences.length){
      sentences(i) = (sentences(i-1)._1 + sentences(i)._1,sentences(i)._2)
    }
    sentences.zipWithIndex.map(x => (x._2,x._1._1,x._1._2))
  }

  def paragraphsWithIndexes(paragraphs: Array[String]): Array[(Int, String)] = {
    paragraphs.filter(_.nonEmpty).filter(_.length > 5).map(replaceNewLinesAndSpaces).zipWithIndex.map(_.swap)
  }

  def sentencesWithIndexes(sentences: Array[String]): Array[(Int, String)] = {
    sentences.filter(_.nonEmpty).zipWithIndex.map(_.swap)
  }

  def ConvertDiacritic(word: String): String = {
    var wordie: String = ""
    try {
      if (word.contains("\\x") && word.length > 1) {
        var ab: ArrayBuffer[Int] = new ArrayBuffer[Int]
        var index: Int = 0
        while (index != -1) {
          index = word.indexOf("\\x", index + 1)
          if (index != -1 && index != 0) {
            val c: String = word.slice(index + 2, index + 4)
            var byte: Int = Integer.parseInt(c, 16)
            ab += byte
          }
        }
        var bytes: ArrayBuffer[Byte] = new ArrayBuffer[Byte]
        for (elem <- ab) {
          bytes += elem.toByte
        }
        val s: String = new String(bytes.toArray, "UTF-8")
        val goodie: String = decompose(s)
        wordie = word.toLowerCase.replaceAll("(\\\\x[a-zA-Z0-9]{0,2})+", goodie)
      }
      else {
        wordie = decompose(word)
      }
    } catch {
      case _: Throwable =>
        wordie = decompose(word)
    }
    wordie
  }

  def decompose(s: String): String = {
    //returns string with special characters replaced
    val str: String = s.map(x => diacr.getOrElse(x, x))

    return java.text.Normalizer.normalize(str, java.text.Normalizer.Form.NFKD)
  }
}
