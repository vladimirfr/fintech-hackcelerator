package fintech.hackcelerator.tools

import java.io.InputStream

import fintech.hackcelerator.config.AppConfig

/** Utility object with helper functions to avoid resource location in a different platforms */
object Resources {
  lazy val fs: org.apache.hadoop.fs.FileSystem = org.apache.hadoop.fs.FileSystem.get(AppConfig.getConfig())

  /** Helper function to get resources from resource project folder as stream */
  def getResourceAsStream(file: String): InputStream = {
    fs.open( new org.apache.hadoop.fs.Path(file))
  }
}
