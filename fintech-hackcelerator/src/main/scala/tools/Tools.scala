package fintech.hackcelerator.tools

import java.security.MessageDigest
import org.apache.commons.codec.binary.Base64
import fintech.hackcelerator.config.AppConfig

object Tools {
  def matchDocTypeByExt(x: String): String = x match {
    case "doc" => "Word"
    case "docx" => "Word"
    case "rtf" => "Word"
    case "pdf" => "PDF"
    case "xls" => "Excel"
    case "xlsx" => "Excel"
    case "xlsm" => "Excel"
    case "ppt" => "Power Point"
    case "pptx" => "Power Point"
    case _ => "Others"
  }

  def matchDocType(x: String, ext_type: Boolean = false): String = {
    if(ext_type){
      return matchDocTypeByExt(x)
    } else {
      return x match {
        case "email" => "Email"
        case _ => matchDocTypeByExt(x)
      }
    }
  }

  /**
    * This method is used to generate hash for string (using SHA-256).
    *
    * @param string input data (String).
    * @return Hash of input string (String).
    */
  def hashString(string: String): String = {
    val hashingAlgorithm = AppConfig.hashingAlgorithm
    val md: MessageDigest = MessageDigest.getInstance(hashingAlgorithm)
    md.update(string.getBytes("UTF-8"))
    return Base64.encodeBase64String(md.digest())
  }
}
