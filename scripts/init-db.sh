#!/bin/bash -x

echo "Creating Hbase Tables"

echo "create_namespace 'Dathena'" | hbase shell

hbase org.apache.hadoop.hbase.util.RegionSplitter 'Dathena:DOCMETA' UniformSplit -f 'Information:Classification:Anomaly' -c 8
hbase org.apache.hadoop.hbase.util.RegionSplitter 'Dathena:DataTypes' UniformSplit -f 'Information' -c 8
hbase org.apache.hadoop.hbase.util.RegionSplitter 'Dathena:USER-DOC' UniformSplit -f 'Information:Contents' -c 8

cat create-db.hbase | hbase shell
cat init-db.hbase | hbase shell

