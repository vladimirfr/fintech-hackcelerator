#!/bin/bash -x

. run_utils.sh

JAR="fintech-hackcelerator-assembly-1.0.jar"
NAME="fintech.hackcelerator"

run_item $NAME.IDriver $JAR
run_item $NAME.NERFind $JAR