export _JAVA_OPTIONS="-Xmx20g"

function run_item() {
  CLASS=$1
  shift
  JAR=$1
  shift
  VERSION=$(hdp-select versions | tail -n1)
  LIBS="/usr/hdp/$VERSION/kafka/libs/guava-18.0.jar:/usr/hdp/$VERSION/phoenix/lib/hbase-protocol.jar:/usr/hdp/$VERSION/hbase/lib/hbase-client.jar:/usr/hdp/$VERSION/hbase/lib/hbase-common.jar:/usr/hdp/$VERSION/phoenix/lib/phoenix-core-4.4.0.$VERSION.jar:/usr/hdp/$VERSION/hbase/lib/hbase-server.jar"

  spark-submit --class $CLASS \
    --conf "spark.cleaner.referenceTracking=false" \
    --conf "spark.driver.extraClassPath=$LIBS" \
    --conf "spark.executor.extraClassPath=$LIBS" \
    --conf "spark.executor.heartbeatInterval=5s" \
    --conf "spark.files=Dathena.conf" \
    --conf "spark.network.timeout=300s" \
    --conf "spark.speculation.multiplier=3" \
    --conf "spark.speculation.quantile=0.80" \
    --conf "spark.speculation=true" \
    --conf "spark.yarn.executor.memoryOverhead=2048" \
    $JAR $@
}
